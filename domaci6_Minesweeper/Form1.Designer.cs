﻿namespace domaci6_Minesweeper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button00 = new System.Windows.Forms.Button();
            this.button01 = new System.Windows.Forms.Button();
            this.button02 = new System.Windows.Forms.Button();
            this.button03 = new System.Windows.Forms.Button();
            this.button04 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.buttonNova = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button00
            // 
            this.button00.Location = new System.Drawing.Point(104, 62);
            this.button00.Name = "button00";
            this.button00.Size = new System.Drawing.Size(45, 36);
            this.button00.TabIndex = 0;
            this.button00.UseVisualStyleBackColor = true;
            this.button00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button00_MouseUp);
            // 
            // button01
            // 
            this.button01.Location = new System.Drawing.Point(155, 62);
            this.button01.Name = "button01";
            this.button01.Size = new System.Drawing.Size(45, 36);
            this.button01.TabIndex = 1;
            this.button01.UseVisualStyleBackColor = true;
            this.button01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button01_MouseUp);
            // 
            // button02
            // 
            this.button02.Location = new System.Drawing.Point(206, 62);
            this.button02.Name = "button02";
            this.button02.Size = new System.Drawing.Size(45, 36);
            this.button02.TabIndex = 2;
            this.button02.UseVisualStyleBackColor = true;
            this.button02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button02_MouseUp);
            // 
            // button03
            // 
            this.button03.Location = new System.Drawing.Point(257, 62);
            this.button03.Name = "button03";
            this.button03.Size = new System.Drawing.Size(45, 36);
            this.button03.TabIndex = 3;
            this.button03.UseVisualStyleBackColor = true;
            this.button03.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button03_MouseUp);
            // 
            // button04
            // 
            this.button04.Location = new System.Drawing.Point(308, 62);
            this.button04.Name = "button04";
            this.button04.Size = new System.Drawing.Size(45, 36);
            this.button04.TabIndex = 4;
            this.button04.UseVisualStyleBackColor = true;
            this.button04.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button04_MouseUp);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(104, 104);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(45, 36);
            this.button10.TabIndex = 5;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button10_MouseUp);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(155, 104);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(45, 36);
            this.button11.TabIndex = 6;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button11_MouseUp);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(206, 104);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(45, 36);
            this.button12.TabIndex = 7;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button12_MouseUp);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(257, 104);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(45, 36);
            this.button13.TabIndex = 8;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button13_MouseUp);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(308, 104);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(45, 36);
            this.button14.TabIndex = 9;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button14_MouseUp);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(104, 146);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(45, 36);
            this.button20.TabIndex = 10;
            this.button20.UseVisualStyleBackColor = true;
            this.button20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button20_MouseUp);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(155, 146);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(45, 36);
            this.button21.TabIndex = 11;
            this.button21.UseVisualStyleBackColor = true;
            this.button21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button21_MouseUp);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(206, 146);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(45, 36);
            this.button22.TabIndex = 12;
            this.button22.UseVisualStyleBackColor = true;
            this.button22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button22_MouseUp);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(257, 146);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(45, 36);
            this.button23.TabIndex = 13;
            this.button23.UseVisualStyleBackColor = true;
            this.button23.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button23_MouseUp);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(308, 146);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(45, 36);
            this.button24.TabIndex = 14;
            this.button24.UseVisualStyleBackColor = true;
            this.button24.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button24_MouseUp);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(104, 188);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(45, 36);
            this.button30.TabIndex = 15;
            this.button30.UseVisualStyleBackColor = true;
            this.button30.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button30_MouseUp);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(155, 188);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(45, 36);
            this.button31.TabIndex = 16;
            this.button31.UseVisualStyleBackColor = true;
            this.button31.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button31_MouseUp);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(206, 188);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(45, 36);
            this.button32.TabIndex = 17;
            this.button32.UseVisualStyleBackColor = true;
            this.button32.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button32_MouseUp);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(257, 188);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(45, 36);
            this.button33.TabIndex = 18;
            this.button33.UseVisualStyleBackColor = true;
            this.button33.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button33_MouseUp);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(308, 188);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(45, 36);
            this.button34.TabIndex = 19;
            this.button34.UseVisualStyleBackColor = true;
            this.button34.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button34_MouseUp);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(104, 230);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(45, 36);
            this.button40.TabIndex = 20;
            this.button40.UseVisualStyleBackColor = true;
            this.button40.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button40_MouseUp);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(155, 230);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(45, 36);
            this.button41.TabIndex = 21;
            this.button41.UseVisualStyleBackColor = true;
            this.button41.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button41_MouseUp);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(206, 230);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(45, 36);
            this.button42.TabIndex = 22;
            this.button42.UseVisualStyleBackColor = true;
            this.button42.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button42_MouseUp);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(257, 230);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(45, 36);
            this.button43.TabIndex = 23;
            this.button43.UseVisualStyleBackColor = true;
            this.button43.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button43_MouseUp);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(308, 230);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(45, 36);
            this.button44.TabIndex = 24;
            this.button44.UseVisualStyleBackColor = true;
            this.button44.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button44_MouseUp);
            // 
            // buttonNova
            // 
            this.buttonNova.Location = new System.Drawing.Point(155, 302);
            this.buttonNova.Name = "buttonNova";
            this.buttonNova.Size = new System.Drawing.Size(147, 23);
            this.buttonNova.TabIndex = 25;
            this.buttonNova.Text = "Nova Igra";
            this.buttonNova.UseVisualStyleBackColor = true;
            this.buttonNova.Click += new System.EventHandler(this.buttonNova_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 356);
            this.Controls.Add(this.buttonNova);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button04);
            this.Controls.Add(this.button03);
            this.Controls.Add(this.button02);
            this.Controls.Add(this.button01);
            this.Controls.Add(this.button00);
            this.Name = "Form1";
            this.Text = "Minesweeper";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button00;
        private System.Windows.Forms.Button button01;
        private System.Windows.Forms.Button button02;
        private System.Windows.Forms.Button button03;
        private System.Windows.Forms.Button button04;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button buttonNova;
    }
}

