﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace domaci6_Minesweeper
{
    public class MinskoPolje : Random
    {
        private bool[,] minskoPolje;
        private int brojMina;


        public MinskoPolje()
        {
            this.minskoPolje = new bool[5, 5];
            this.brojMina = 3;
            while (this.brojMina > 0)
            {
                int i = Next(0, 4);
                int j = Next(0, 4);
                if (minskoPolje[i, j] == true)
                {
                    return;
                }
                else
                {
                    minskoPolje[i, j] = true;
                    this.brojMina--;
                }
            }
        }

        public bool MinaPostoji(int i, int j)
        {
            if(minskoPolje[i,j] == true)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public int BrojMina(int i, int j)
        {
            int broj = 0;
            if(i == 0 && j == 0)
            {
                for (int k = 0; k < 2; k++)
                {
                    for (int l = 0; l < 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                            
                    }
                        
                }
                    
            }else if (i == 0 && j == 4)
            {
                for (int k = 0; k < 2; k++)
                {
                    for (int l = 4; l >= 3; l--)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }

                    }

                }

            }else if (i == 4 && j == 0)
            {
                for (int k = 4; k >= 3; k--)
                {
                    for (int l = 0; l < 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }

                    }

                }

            }else if (i == 4 && j == 4)
            {
                for (int k = 3; k < 5; k++)
                {
                    for (int l = 3; l < 5; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }

                    }

                }

            }
            else if(i == 0 && j > 0 && j < 4)
            {
                for (int k = 0; k < 2; k++)
                {
                    for (int l = j - 1; l < j + 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                    }
                }

            }else if (j == 0 && i > 0 && i < 4)
            {
                for (int k = i - 1; k < i + 2; k++)
                {
                    for (int l = 0; l < 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                    }
                }

            }else if (i == 4 && j > 0 && j < 4)
            {
                for (int k = 3; k < 5; k++)
                {
                    for (int l = j - 1; l < j + 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                    }
                }

            }else if (j == 4 && i > 0 && i < 4)
            {
                for (int k = i - 1; k < i + 2; k++)
                {
                    for (int l = 3; l < 5; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                    }
                }

            }else if ((i > 0 || i < 4) && (j > 0 || j < 4))
            {
                for (int k = i - 1; k < i + 2; k++)
                {
                    for (int l = j + 1; l < j + 2; l++)
                    {
                        if (k == i && l == j)
                        {
                            continue;
                        }
                        else
                        {
                            if (minskoPolje[k, l] == true)
                                broj++;
                        }
                    }
                }
            }
            return broj;
        }
    }
}
