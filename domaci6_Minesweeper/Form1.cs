﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace domaci6_Minesweeper
{
    public partial class Form1 : Form
    {
        MinskoPolje minskoPolje;
        Button[,] buttons;
        public Form1()
        {
            InitializeComponent();
            minskoPolje = new MinskoPolje();
            buttons = new Button[5, 5];
            buttons[0, 0] = button00;
            buttons[0, 1] = button01;
            buttons[0, 2] = button02;
            buttons[0, 3] = button03;
            buttons[0, 4] = button04;
            buttons[1, 0] = button10;
            buttons[1, 1] = button11;
            buttons[1, 2] = button12;
            buttons[1, 3] = button13;
            buttons[1, 4] = button14;
            buttons[2, 0] = button20;
            buttons[2, 1] = button21;
            buttons[2, 2] = button22;
            buttons[2, 3] = button23;
            buttons[2, 4] = button24;
            buttons[3, 0] = button30;
            buttons[3, 1] = button31;
            buttons[3, 2] = button32;
            buttons[3, 3] = button33;
            buttons[3, 4] = button34;
            buttons[4, 0] = button40;
            buttons[4, 1] = button41;
            buttons[4, 2] = button42;
            buttons[4, 3] = button43;
            buttons[4, 4] = button44;

        }

        public void Akcija(int i, int j, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (buttons[i, j].BackColor == Color.Black)
                    buttons[i, j].BackColor = Color.White;
                else
                    buttons[i, j].BackColor = Color.Black;
            }
            else
            {
                minskoPolje.MinaPostoji(i, j);
                if (minskoPolje.MinaPostoji(i, j))
                {
                    buttons[i, j].BackColor = Color.Red;
                    MessageBox.Show("Nagazili ste minu!", "Kraj");
                    Close();
                }
                else
                {
                    int brojMina = minskoPolje.BrojMina(i, j);
                    if (brojMina == 0)
                    {
                        brojMina = minskoPolje.BrojMina(i, j);
                        buttons[i, j].Enabled = false;
                    }
                    buttons[i, j].Text = brojMina.ToString();
                    buttons[i, j].Enabled = false;
                }
                if (Zavrsnica())
                {
                    MessageBox.Show("Cestitamo", "Kraj");
                }


            }
        }

        public bool Zavrsnica()
        {

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (buttons[i, j].Enabled || !(buttons[i, j].BackColor == Color.Black))
                        return false;
                }
            }
            return true;
        }

        private void buttonNova_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    buttons[i, j].Enabled = true;
                    buttons[i, j].BackColor = Color.White;
                    buttons[i, j].Text = "";
                }
            }
        }

        private void button00_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(0, 0, e);
        }

        private void button01_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(0, 1, e);
        }

        private void button02_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(0, 2, e);
        }

        private void button03_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(0, 3, e);
        }

        private void button04_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(0, 4, e);
        }

        private void button10_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(1, 0, e);
        }

        private void button11_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(1, 1, e);
        }

        private void button12_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(1, 2, e);
        }

        private void button13_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(1, 3, e);
        }

        private void button14_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(1, 4, e);
        }

        private void button20_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(2, 0, e);
        }

        private void button21_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(2, 1, e);
        }

        private void button22_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(2, 2, e);
        }

        private void button23_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(2, 3, e);
        }

        private void button24_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(2, 4, e);
        }

        private void button30_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(3, 0, e);
        }

        private void button31_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(3, 1, e);
        }

        private void button32_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(3, 2, e);
        }

        private void button33_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(3, 3, e);
        }

        private void button34_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(3, 4, e);
        }

        private void button40_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(4, 0, e);
        }

        private void button41_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(4, 1, e);
        }

        private void button42_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(4, 2, e);
        }

        private void button43_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(4, 3, e);
        }

        private void button44_MouseUp(object sender, MouseEventArgs e)
        {
            Akcija(4, 4, e);
        }
    }
}
